import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ToastController, Events } from 'ionic-angular';
import { GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions, CameraPosition, MarkerOptions, Marker } from '@ionic-native/google-maps';
import { MapProvider } from '../../providers/map/map';
import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the MapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {
  map: GoogleMap;
  lat:number;
  lng:number;
  query:string;
  consulta;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public MapProvider:MapProvider,
    private toastCtrl:ToastController,
    public evts:Events,
    public platform:Platform,
    private geolocation: Geolocation) {
    // this.platform.ready().then(
    //   ()=>{
    //     this.loadMap();

    //   }
    // );


  }

  ionViewDidLoad() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.lat =  resp.coords.latitude;
      this.lng =  resp.coords.longitude;
      this.loadMap();
     }).catch((error) => {
       console.log('Error getting location', error);
     });

    this.getData();
  }
  loadMap() {

    let mapOptions: GoogleMapOptions = {
      camera: {
         target: {
           lat: this.lat,
           lng: this.lng
         },
         zoom: 18,
         tilt: 30
       }
     };
 
    this.map = GoogleMaps.create('map_canvas', mapOptions);
 
    // Wait the MAP_READY before using any methods.
    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
         console.log('Map is ready!');
 
         // Now you can use all methods safely.
         this.map.addMarker({
           title: 'DMfusion',
           icon: 'blue',
           animation: 'DROP',
           position: {
            lat: this.lat,
            lng: this.lng
           }
         },
        )
         .then(marker => {
           marker.on(GoogleMapsEvent.MARKER_CLICK)
             .subscribe(() => {
               alert('este es DMfusion');
             });
         });
 
      });
   }

  getData(){
    this.MapProvider.getData( this.lat, this.lng, this.query )    
    .subscribe(
        data => {
            if(!data.error){
                console.log(data);
                this.consulta = data;
                console.log("entra");
            }
        },
        error => {
            if (error.status==401) {
                this.evts.publish("user:logout");
            } else {
                this.showError("Error de conexión");
            }
        }
    );
  }

  private showError(message){
    let toast = this.toastCtrl.create({
        message: message,
        duration: 3000,
        position: 'middle'
    });

    toast.onDidDismiss(() => {
        
    });

    toast.present();
  }

}
