import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, AlertController, ModalController, Platform, ToastController, Events  } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { NoticiasProvider } from '../../providers/noticias/noticias';

import { NoticiaPage } from '../../pages/noticia/noticia';
import { MapPage } from '../../pages/map/map';
/**
 * Generated class for the ListaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lista',
  templateUrl: 'lista.html',
  providers: [UserProvider]
})
export class ListaPage {
  noticias;
  sinNoticia:Boolean;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    private usrProvider:UserProvider,
    public noticiasProvider:NoticiasProvider,
    public evts:Events,
    private toastCtrl:ToastController) {
    // this.menuCtrl.enable(true, 'menu-login');
    // this.menuCtrl.enable(false, 'menu-logout');
    this.noticiasProvider.getNoticias()    
    .subscribe(
        data => {
            if(!data.error){
                console.log(data.data);
                this.noticias = data.data;
                this.sinNoticia =  (Object.keys(this.noticias).length==0) ? true : false ;
            }
        },
        error => {
            if (error.status==401) {
                this.evts.publish("user:logout");
            } else {
                this.showError("Error de conexión");
            }
        }
    );
  }
    private showError(message){
      let toast = this.toastCtrl.create({
          message: message,
          duration: 3000,
          position: 'middle'
      });

      toast.onDidDismiss(() => {
          
      });

      toast.present();
  }
    private abrirNoticia(data){
        this.navCtrl.push(NoticiaPage, data);
      
    }
  ionViewDidLoad() {
    // console.log('ionViewDidLoad ListaPage');
  }

}
