import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ToastController, Platform, ModalController, MenuController } from 'ionic-angular';
import { SignupPage } from '../../pages/signup/signup';
import { ListaPage } from '../../pages/lista/lista';
import { UserProvider } from '../../providers/user/user';
import { AppConfig } from '../../config/config';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [UserProvider]
})
export class LoginPage {
  user:string;
  password:string;
  number: number;

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      private toastCtrl:ToastController, 
      public platform: Platform,
      public modalCtrl: ModalController,
      private usrProvider:UserProvider,
      public evts:Events,
      public menuCtrl: MenuController
    ) {
        this.menuCtrl.enable(false, 'menu-login');
        this.menuCtrl.enable(true, 'menu-logout');
    }
    
    ionViewDidLoad() {
    // console.log(this.usrProvider.getMethod());
    // console.log('ionViewDidLoad LoginPage');
  }
  public signup(){
    this.navCtrl.push(SignupPage);
    // this.navCtrl.setRoot(ListaPage);
  }

  public logInUser(){
    if(this.user !== undefined && this.password !== undefined){
        this.usrProvider.logInUser(this.user, this.password)
            .subscribe(
                data => {
                    if(!data.error){
                        this.logIn(data);
                    }
                },
                error => {
                    if (error.status==401) {
                        this.showError("Credenciales inválidas");
                    } else {
                        this.showError("Error de conexión");
                    }
                }
            )
    }else{
        if(this.number === undefined){
            this.showError("Debes ingresar el Email.");
        }else if(this.password === undefined){
            this.showError("Debes ingresar tu contraseña.");
        }
    }
} 
private logIn(data){
  this.usrProvider.setSession(data);
  this.evts.publish("user:login");

  let _msg = "Bienvenido "+data.message.name;

  let toast = this.toastCtrl.create({
      message: _msg,
      duration: 2000,
      position: 'middle'
  });
 //accion luego de mostrar el toast.
  toast.onDidDismiss(() => {
    // this.navCtrl.setRoot(ListaPage);
  });
  //ejecutar Toast.
  toast.present();
}

private showError(message){
    let toast = this.toastCtrl.create({
        message: message,
        duration: 3000,
        position: 'middle'
    });

    toast.onDidDismiss(() => {
        
    });

    toast.present();
}

}
