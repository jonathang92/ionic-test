import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the NoticiaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-noticia',
  templateUrl: 'noticia.html',
})
export class NoticiaPage {
  titulo:string;
  preview:string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.titulo = this.navParams.get('titulo');
    this.preview = this.navParams.get('preview');
  }
  ionViewDidLoad() {
    // console.log('ionViewDidLoad NoticiaPage');
  }

}
