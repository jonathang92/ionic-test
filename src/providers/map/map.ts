import { HttpClient } from '@angular/common/http';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { AppConfig } from '../../config/config';
import 'rxjs/add/operator/map';
import { UserProvider } from '../../providers/user/user';

/*
  Generated class for the MapProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MapProvider {

  user;

  constructor(public http: Http,
    public usrService:UserProvider) {
    // console.log('Hello MapProvider Provider');
  }
  public getData(lat,lng,query){
    this.user = this.usrService.getSession();
    if (this.user.length>0) {
    }

    let data: Object = {
      lat: lat,
      lng: lng,
      query: query
    };

    let headers = new Headers({ 
      'Authorization': 'Bearer '+this.user.token, 
      'Accept': 'application/json',
      // 'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
      });
      
      // let headers = new Headers({ 'Content-Type': 'application/json'});
      let options = new RequestOptions({ headers: headers });

      return this.http.post(AppConfig.API_ENDPOINT() + "map", data, options).map(
          res => res.json(),
          error => {
              return {error:true, message:error};
          }
      );
  }
}
