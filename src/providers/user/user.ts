import { HttpClient } from '@angular/common/http';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { AppConfig } from '../../config/config';
import 'rxjs/add/operator/map';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {
  _session;
  client;

  constructor(public http: Http) {
    // this._session     = this.getSession();
    this.client = AppConfig.CLIENT_API();
  }

  logInUser(user: any, pass: any): any {
    let data: Object = {
      username: user,
      password: pass,
      grant_type: "password",
      client_id: this.client.id,
      client_secret: this.client.secret
    };
    let headers = new Headers({ 'Content-Type': 'application/json'});
    let options = new RequestOptions({ headers: headers });
    
    return this.http.post(AppConfig.API_ENDPOINT()+'login', data, options).map(
        res => res.json(),
        error => {
            return {error};
        }
    );
  }

  public setSession(data):void{
    let sessionData: Array<{ 
        session:boolean, 
        user: number, 
        name: string, 
        email: string, 
        token:string,
    }>;

    let depura = (data.depura_pendiente == "1") ? true : false;

    sessionData = [{
        session: true, 
        user: data.message.id, 
        name: data.message.name,
        email: data.message.email,
        token: data.data.token
    }];
    localStorage.setItem(AppConfig.SESSION_KEY(), JSON.stringify(sessionData));
  } 
  //devuelve los datos de sesión
  public getSession(){
    let sessionData: string | boolean;
    sessionData = localStorage.getItem(AppConfig.SESSION_KEY());

    return (sessionData != null) ? JSON.parse(sessionData)[0] : false;
  }
    //devuelve si el usuario esta logeado o no
  public isLogged(){
    return (this.getLoggedUser()==false) ? false : true ;
  }
  //devuelve data del usuario almacenada previamente
  public getLoggedUser(){
    let sessionData: any;
    sessionData = localStorage.getItem(AppConfig.SESSION_KEY());
    sessionData = JSON.parse(sessionData);

    sessionData = (sessionData !== null && sessionData[0] && sessionData[0].user && sessionData[0].user != -1) ? sessionData[0].user : false;

    return sessionData;
  }
  //elimina la info del usuario
  public removeSession():void{
    //this.storage.remove(AppConfig.SESSION_KEY());
    let sessionData: string | boolean;
    sessionData = localStorage.getItem(AppConfig.SESSION_KEY());

    localStorage.removeItem(AppConfig.SESSION_KEY());
} 

}
