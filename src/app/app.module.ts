import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleMaps } from "@ionic-native/google-maps";

import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
// Components
import { HeaderComponent } from '../components/header/header';
// Pages
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { ListaPage } from '../pages/lista/lista';
import { NoticiaPage } from '../pages/noticia/noticia';
import { MapPage } from '../pages/map/map';
// Providers
import { UserProvider } from '../providers/user/user';
import { NoticiasProvider } from '../providers/noticias/noticias';
import { MapProvider } from '../providers/map/map';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    SignupPage,
    ListaPage,
    NoticiaPage,
    MapPage,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    SignupPage,
    ListaPage,
    NoticiaPage,
    MapPage,
    HeaderComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    GoogleMaps,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserProvider,
    NoticiasProvider,
    NoticiasProvider,
    MapProvider
  ]
})
export class AppModule {
  
}
