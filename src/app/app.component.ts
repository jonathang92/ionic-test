import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, MenuController, Events, AlertController, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UserProvider } from '../providers/user/user';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { ListaPage } from '../pages/lista/lista';
import { NoticiaPage } from '../pages/noticia/noticia';
import { MapPage } from '../pages/map/map';

//components
import { HeaderComponent } from '../components/header/header';
import { Subject } from 'rxjs/Subject';
@Component({
  templateUrl: 'app.html',
  providers: [UserProvider]
})
export class MyApp {
  @ViewChild('nav') navCtrl: NavController
  rootPage: any;
  user: number;
  activePage = new Subject();
  pages: Array<{ title: string, component: any, active: boolean, icon: string, type: string }>;
  constructor(
    public platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public menuCtrl: MenuController,
    private usrProvider:UserProvider,
    public evt:Events,
    public alertCtrl: AlertController,
    public toastCtrl:ToastController) {
      platform.ready().then(() => {
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        // platform.registerBackButtonAction(()=>this.myHandlerFunction()); //"presione otra vez para salir."
        this.rootPage = this.ifLogin();
        statusBar.styleDefault();
        splashScreen.hide();
        this.setearMenu();
        this.evt.subscribe("user:login", () => {
          this.user = this.usrProvider.getLoggedUser();
          this.navCtrl.setRoot(ListaPage);
          this.setearMenu();
        });
        this.evt.subscribe("user:logout", () => {
          this.logout();
          let toast = this.toastCtrl.create({
            message: "Debes iniciar sesión nuevamente.",
            duration: 2000,
            position: 'middle'
          });
          toast.present();
        });
      });
      this.activePage.subscribe((selectedPage: any) => {
        this.pages.map(page => {
          page.active = page.title === selectedPage.title;
        });
      });
    }
    // public myHandlerFunction(){
    //   let toast = this.toastCtrl.create({
    //    message: "Press Again to Confirm Exit",
    //    duration: 3000
    //  });
    //  toast.present(); 
    //   }
    public ifLogin(){
      return (this.usrProvider.getLoggedUser()) ? ListaPage :LoginPage;
      
    }
    public setearMenu(){
      // console.log(this.navCtrl.getActive().component.name);
      if(!this.usrProvider.getLoggedUser()){
        this.menuCtrl.enable(false, 'menu-app');
        this.pages = [
          { title: 'SignUp', component: SignupPage, active: true, icon: 'home', type: 'push' },
        ];
      } else {
        this.menuCtrl.enable(true, 'menu-app');
          this.pages = [
            { title: 'List', component: ListaPage, active: true, icon: 'home', type: 'root' },
            { title: 'Map', component: MapPage, active: true, icon: 'home', type: 'root' },
            { title: 'Logout', component: SignupPage, active: true, icon: 'home', type: 'logout' },
          ];

      }
    }
    public logout(){
      this.usrProvider.removeSession();		
      this.setearMenu();
      this.navCtrl.setRoot(LoginPage);        
    }
    public openPage(page) {
      // Reset the content nav to have just this page
      // we wouldn't want the back button to show in this scenario
      if (page.type =="push") {
        this.navCtrl.popToRoot();
        this.navCtrl.push(page.component);
      } else if (page.type =="root") {
        this.navCtrl.setRoot(page.component);
      } else if (page.type =="logout") {
        this.logout();
      }
      this.activePage.next(page);
    }
}

